let prices = {
    "type1":{
        price1: 18
    },
    "type2":{
        price2: 23,
        options: {
            "option1": 12,
            "option2": 13,
            "option3": 14
        }
    },
    "type3":{
        price3: 48,
        svoistva:{
            "svoistvo1": 3,
            "svoistvo2": 4,
            "svoistvo3": 5
        }
    }
};
let select1;
let radios1;
let checkboxes1;
let amount1;
function raschitat(){
let options = document.getElementById("options");
let checkboxes = document.getElementById("checkboxes");
let priceee=0;

if (select1.value === "type1"){
    checkboxes.style.display="none";
    options.style.display="none";
    priceee += prices.type1.price1;
}
if (select1.value==="type2"){
    checkboxes.style.display="none";
    options.style.display="block";
    priceee += prices.type2.price2;
    radios1.forEach(function(optii){
        if (optii.checked){
            priceee += prices.type2.options[optii.value];
        }
    });
}
if (select1.value==="type3"){
    checkboxes.style.display="block";
    options.style.display="none";
    priceee += prices.type3.price3;
    checkboxes1.forEach(function(svoistva2){
        if (svoistva2.checked){
            priceee += prices.type3.svoistva[svoistva2.value];
        }
    });
}


 if(!(/^(0|[1-9][0-9]*)$/).test(amount1.value)){
   document.getElementById("stoimost").innerHTML = "Введите адекватное количество";
        return;
  } else  {  
document.getElementById("stoimost").innerHTML = "Стоимость равна: " + priceee*amount1.value + " р.";
}
}
window.addEventListener("DOMContentLoaded", function(){
 select1 = document.getElementById("type");
 radios1 = document.getElementsByName("options");
 checkboxes1 = document.getElementsByName("svoistvo");
 amount1 = document.getElementById("amount");
select1.addEventListener("change", raschitat);
radios1.forEach(function(optii){
    optii.addEventListener("change", raschitat);
});
checkboxes1.forEach(function(svoistva2){
    svoistva2.addEventListener("change", raschitat);
});
amount1.addEventListener("change", raschitat);
raschitat();
});
